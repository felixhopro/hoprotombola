# Examples

## Math with LaTeX

$$\int_0^{\infty} x^2\, dx$$

## Adminitions
???+ ph-goal "Learning GOals"

    - ...


???+ ph-notes "Notes"
    
    ...



???+ ph-solution "Solutions"
    
    ...


???+ ph-skill "Skills"
    
    ...


???+ ph-question "Question"
    
    ...

    ??? ph-sol "Answer"
        
        ...



??? ph-link "Additional Materials"
    
    ...


## Terminal with Termynal

<div align="center" data-termynal-container>
    <div id="termynal" data-termynal="" data-ty-typedelay="40" data-ty-lineDelay="1000">
        <span data-ty="input">pip install mathy</span>
        <span data-ty="progress"></span>
        <span class="u-hide-sm" data-ty-lineDelay="0" data-ty="">Successfully installed mathy</span>
        <span data-ty-lineDelay="0" class="u-hide-sm" data-ty=""></span>
        <span data-ty="input">mathy simplify "2x + 1y^3 + 7b + 4x"</span>
        <span data-ty="" data-ty-text="initial                   | 2x + 1y^3 + 7b + 4x"></span>
        <span data-ty="" data-ty-text="associative group         | 2x + (1y^3 + 7b) + 4x"></span>
        <span data-ty="" data-ty-text="associative group         | 2x + (1y^3 + 7b + 4x)"></span>
        <span data-ty="" data-ty-text="commutative swap          | 1y^3 + 7b + 4x + 2x"></span>
        <span data-ty="" data-ty-text="associative group         | 1y^3 + 7b + (4x + 2x)"></span>
        <span data-ty="" data-ty-text="distributive factoring    | 1y^3 + 7b + (4 + 2) * x"></span>
        <span data-ty="" data-ty-text="constant arithmetic       | 1y^3 + 7b + 6x"></span>
        <span data-ty-lineDelay="0" class="u-hide-sm" data-ty=""></span>
        <span data-ty="" data-ty-text='"2x + 1y^3 + 7b + 4x" = "1y^3 + 7b + 6x"'></span>
    </div>
</div>